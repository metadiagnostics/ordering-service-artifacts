<?xml version="1.0" encoding="utf-8"?>
<xs:schema targetNamespace="http://www.omg.org/specs/HCOS/orderservice/types.xsd"  xmlns="http://www.omg.org/specs/HCOS/orderservice/types.xsd" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:include schemaLocation="order_service_common_classes.xsd"/>
	<xs:element name="SubjectReference" type="SubjectReference"/>
	<xs:complexType name="SubjectReference">
		<xs:annotation>
			<xs:documentation>Reference to the Subject of the order (e.g. Patient). </xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="Reference">
				<xs:sequence>
					<xs:element name="OrderResponse" type="OrderResponse" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="Fulfillment" type="Fulfillment"/>
	<xs:complexType name="Fulfillment">
		<xs:annotation>
			<xs:documentation>Information relating to the fulfillment of the order items within an order</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="FulfillmentIdentity" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a fulfillment status</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderIdentity" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a request for performance of order(s)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="IdentityOrderedItems" type="Identifier" minOccurs="1" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>The codes for the testing being requested</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PromisedItems" type="OrderItem" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="Status" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Values for the status attribute are taken from the concept domain LabBusinessProcessStatus</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Subject" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for the subject of the testing (the source of the specimen)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PromisedItems" type="OrderItem" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Workflow" type="Workflow"/>
	<xs:complexType name="Workflow">
		<xs:annotation>
			<xs:documentation>Wrapper of a workflow. The ContentSignifier indicates the form of workflow model that is found encoded in the Content field. </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Content" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="StatisticsCollection" type="StatisticsCollection"/>
	<xs:complexType name="StatisticsCollection">
		<xs:annotation>
			<xs:documentation>A collection of statistics.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Statistics" type="Statistic" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="CommonRationale" type="CommonRationale"/>
	<xs:complexType name="CommonRationale">
		<xs:annotation>
			<xs:documentation>Abstract class that for Rationale</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Type" type="xs:string" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="RequirementStatus" type="RequirementStatus"/>
	<xs:complexType name="RequirementStatus">
		<xs:annotation>
			<xs:documentation>This class is used to capture the status and state of a requirement, e.g. "signed" and the digital signature of the signer.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="RequirementIdentifier" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Status" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Content" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Rationale" type="Rationale"/>
	<xs:complexType name="Rationale">
		<xs:annotation>
			<xs:documentation>Documentation for the rationale behind an order.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Content" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="DetailedTypeSupport" type="DetailedTypeSupport"/>
	<xs:complexType name="DetailedTypeSupport">
		<xs:annotation>
			<xs:documentation>List of type support that includes the identity of the fulfulliers supporting that type.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="TypeSupport">
				<xs:sequence>
					<xs:element name="Fullfilers" type="Identifier" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="OrderCheckResult" type="OrderCheckResult"/>
	<xs:complexType name="OrderCheckResult">
		<xs:annotation>
			<xs:documentation>The results of checking an order for potential fulfillment. </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Valid" type="xs:boolean" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Fulfillable" type="xs:boolean" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Problems" type="OrderFaults" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ProposedOrderItem" type="ProposedOrderItem"/>
	<xs:complexType name="ProposedOrderItem">
		<xs:annotation>
			<xs:documentation>Association to fulfillment information relating to this order item. The fulfillment associations will evolve as the order progresses. Unreleased orders may have user/system selected or potential fulfillers. As the lifecycle of the order proceeds the associations will reflect the actual fulfillers and acts of fulfillment.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="OrderItem">
				<xs:sequence>
					<xs:element name="ProposedFulfillment" type="ProposedFulfillment" minOccurs="0" maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="RequirementAssociation" type="RequirementAssociation"/>
	<xs:complexType name="RequirementAssociation">
		<xs:sequence>
			<xs:element name="RequirementIdentifier" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="TargetType" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="TargetIdentifier" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Promise" type="Promise" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="OrderItem" type="OrderItem" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Requirement" type="Requirement" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderItem" type="OrderItem"/>
	<xs:complexType name="OrderItem">
		<xs:annotation>
			<xs:documentation>A specific element to order. For example a medication.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="OrderItemIdentifier" type="Identifier" minOccurs="0" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Identifier for the OrderItem</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ItemType" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The type of item being ordered (e.g. medication, lab, etc)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Indication of the type and form for what is in the encoded in the Content field</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Content" type="xs:hexBinary" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The encoded content of the item (e.g. FHIR Medication resource, CDS document etc)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Requirements" type="RequirementStatus" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Association to requirements and their status that are relative to this order item.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Requirement" type="Requirement" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Fulfillers" type="Fulfillment" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="FulfillmentAssociation" type="FulfillmentAssociation"/>
	<xs:complexType name="FulfillmentAssociation">
		<xs:annotation>
			<xs:documentation>Provides an association that covers the life-cycle of the fulfillment of an order item. In the earlier part of the order life-cycle this association is used to provide suggestions and guidance for how the order may be fulfilled. In the later phases of an order life-cycle this association would  reflect both what is pending fulfillment and a history for fulfillments.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Fulfiller" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Fulfillment" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="FulfillmentDetailType" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="FulfillmentDetail" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
			<xs:element name="FulfillmentPeriod" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderItem" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Preferred" type="xs:boolean" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Selected" type="xs:boolean" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Status" type="xs:string" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ServiceStatus" type="ServiceStatus"/>
	<xs:complexType name="ServiceStatus">
		<xs:annotation>
			<xs:documentation>General information about the service instance</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Status" type="Coding" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Version" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="DateStarted" type="DateTime" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderDetail" type="OrderDetail"/>
	<xs:complexType name="OrderDetail">
		<xs:annotation>
			<xs:documentation>This class is used to communicate details about the subject or the order necessary for the performance of the order. This includes supplemental order detail for the orders that require an extended common context between the ordered items. For example specimen information.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Content" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Order" type="Order"/>
	<xs:complexType name="Order">
		<xs:annotation>
			<xs:documentation>This class represents the order as a whole.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="OrderIdentities" type="xs:string" minOccurs="1" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Unique identifiers for this instantiation of a request for performance of order</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderDetails" type="OrderDetail" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Information relevant for the performance of an order.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderedItems" type="OrderedItem" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>The actual work being ordered</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderedBy" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Authorized individual responsible for the order </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderTime" type="DateTime" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The time the order was released for Fulfillment.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Status" type="CodeableConcept" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Overall status of the order</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Subject" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The subject who is concern of the order</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="InterestedParty" type="Reference" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Reference(s) to other parties (Clinical Practitioners, Organizations, etc) that have an interest in the outcome of this order.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderProvenance" type="Provenance" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="Rationale" type="Rationale" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Reasoning explaining why this order is being placed.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Priority" type="Coding" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The criticality of the order.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Context" type="OrderContext" minOccurs="0" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The context in which this order is occurring.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ProposedItems" type="ProposedOrderItem" minOccurs="1" maxOccurs="1"/>
			<xs:element name="PromissedItems" type="PromissedItem" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="CatalogEntry" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderResponse" type="OrderResponse" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderDetails" type="OrderDetail" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="Rationale" type="Rationale" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="FulfillmentAssignment" type="FulfillmentAssignment"/>
	<xs:complexType name="FulfillmentAssignment">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="OrderResponse" type="OrderResponse"/>
	<xs:complexType name="OrderResponse">
		<xs:annotation>
			<xs:documentation>Information relating to the performance of the order. For example laboratory results, or the analysis of an image. </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ResponseIdentity" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for an instantiation of a lab report, lab results which are formatted in the form of a report or electronic document.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Subject" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for the subject of the testing (the source of the specimen)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="FulfillmentIdentity" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a fulfillment status</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderedItems" type="OrderItem" minOccurs="1" maxOccurs="unbounded"/>
			<xs:element name="OrderIdentity" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a request for performance of order(s)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PromisedItems" type="OrderItem" minOccurs="1" maxOccurs="unbounded"/>
			<xs:element name="PromisesIdentity" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a promise or intent to perform.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Results" type="xs:string" minOccurs="1" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>For diagnostics like Laboratory, this class represents the 'answer' to the quantative or qualitative request.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Status" type="xs:string" minOccurs="1" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>Values for the status attribute are taken from the concept domain ActStatus</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PromisedItems" type="OrderItem" minOccurs="1" maxOccurs="unbounded"/>
			<xs:element name="OrderedItems" type="OrderItem" minOccurs="1" maxOccurs="unbounded"/>
			<xs:element name="Result" type="Result" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="FulfilmentInfo" type="FulfilmentInfo"/>
	<xs:complexType name="FulfilmentInfo">
		<xs:annotation>
			<xs:documentation>Container to provide a collection or known fulfillers as well as an index of what orders types are supported.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Fulfillers" type="FulfillerInfo" minOccurs="1" maxOccurs="1"/>
			<xs:element name="SupportedItemsTypes" type="DetailedTypeSupport" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Fulfillers" type="FulfillerInfo" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="SupportedItemsTypes" type="DetailedTypeSupport" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="FulfillerInfo" type="FulfillerInfo"/>
	<xs:complexType name="FulfillerInfo">
		<xs:annotation>
			<xs:documentation>Information about a specific fulfiller including the order item types they support.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Name" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="FulfillerIdentifier" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Description" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Status" type="Coding" minOccurs="1" maxOccurs="1"/>
			<xs:element name="SupportedTypes" type="TypeSupport" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ResultAugmentation" type="ResultAugmentation"/>
	<xs:complexType name="ResultAugmentation">
		<xs:annotation>
			<xs:documentation>Supplemental information about an order response or result. </xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="FulfillmentIdentity" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderIdentity" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Type" type="Coding" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Context" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="EvidenceBasedRationalReference" type="EvidenceBasedRationalReference"/>
	<xs:complexType name="EvidenceBasedRationalReference">
		<xs:annotation>
			<xs:documentation>Rationale for the order is Evidence based and this data structure is used to reference it.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="CommonRationale">
				<xs:sequence>
					<xs:element name="Reference" type="xs:string" minOccurs="1" maxOccurs="1"/>
					<xs:element name="OrderCatalogId" type="xs:string" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="NoSuchOrder" type="NoSuchOrder"/>
	<xs:complexType name="NoSuchOrder">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="TypeSupport" type="TypeSupport"/>
	<xs:complexType name="TypeSupport">
		<xs:annotation>
			<xs:documentation>Information on the support ContentSignifiers supported on a type.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Type" type="Coding" minOccurs="1" maxOccurs="1"/>
			<xs:element name="SupportedContents" type="ContentSignifier" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ProposedFulfillment" type="ProposedFulfillment"/>
	<xs:complexType name="ProposedFulfillment">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="SubjectiveRationale" type="SubjectiveRationale"/>
	<xs:complexType name="SubjectiveRationale">
		<xs:annotation>
			<xs:documentation>Rationale for the order is subjective.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="CommonRationale">
				<xs:sequence>
					<xs:element name="Rationale" type="Text" minOccurs="1" maxOccurs="1">
						<xs:annotation>
							<xs:documentation>Documentation of the subjective rationale</xs:documentation>
						</xs:annotation>
					</xs:element>
					<xs:element name="Author" type="ClinicalPractitionerReference" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="OrderContext" type="OrderContext"/>
	<xs:complexType name="OrderContext">
		<xs:annotation>
			<xs:documentation>The context in which the order is occurring.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="EpisodeOfCareReference" type="Reference" minOccurs="1" maxOccurs="1"/>
			<xs:element name="EncounterReference" type="Reference" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="SubstitutionProposal" type="SubstitutionProposal"/>
	<xs:complexType name="SubstitutionProposal">
		<xs:annotation>
			<xs:documentation>The class represent a proposal by a fulfillment service do something other then exactly what was requested. For example the substitution of a generic drug for a name brand one.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ProposalIdentity" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderIdentity" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ProposalFromFulfiller" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OriginalItem" type="OrderItem" minOccurs="0" maxOccurs="1"/>
			<xs:element name="ProposedReplacements" type="OrderItem" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="RelatedProposals" type="Identifier" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="ProposalState" type="CodeableConcept" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ReasonForProposal" type="CodeableConcept" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ProposalDate" type="DateTime" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ExpirationDate" type="DateTime" minOccurs="0" maxOccurs="1"/>
			<xs:element name="OriginalItem" type="OrderItem" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ProposedReplacements" type="OrderItem" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="QueryFilter" type="QueryFilter"/>
	<xs:complexType name="QueryFilter">
		<xs:annotation>
			<xs:documentation>Definition of filters to apply to a search or find operation.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Subjects" type="xs:string" minOccurs="0" maxOccurs="1"/>
			<xs:element name="OrderBy" type="ClinicalPractitionerReference" minOccurs="0" maxOccurs="1"/>
			<xs:element name="FromOrderRequestDate" type="DateTime" minOccurs="0" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Equated to Order Request Time</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ToOrderRequestDate" type="DateTime" minOccurs="0" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Equated to Order Request Time</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Status" type="Coding" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="Type" type="Coding" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Statistic" type="Statistic"/>
	<xs:complexType name="Statistic">
		<xs:annotation>
			<xs:documentation>A single statistic, identified by code.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Statistic" type="Coding" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>A Coding the uniquely identifies the statistic</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Value" type="xs:double" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Teh value of the statistic</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Context" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>A context to understand the statistic in, for example the fulfillers identifier</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderFaultInfo" type="OrderFaultInfo"/>
	<xs:complexType name="OrderFaultInfo">
		<xs:annotation>
			<xs:documentation>Information that targets a specific fault in an order to am order item.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Problem" type="CodeableConcept" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderItem" type="OrderItem" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderFaults" type="OrderFaults"/>
	<xs:complexType name="OrderFaults">
		<xs:annotation>
			<xs:documentation>A List of faults with an order</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Faults" type="FaultList" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderedItem" type="OrderedItem"/>
	<xs:complexType name="OrderedItem">
		<xs:complexContent>
			<xs:extension base="OrderItem">
				<xs:sequence>
					<xs:element name="FulfillmentAssignments" type="FulfillmentAssignment" minOccurs="0" maxOccurs="unbounded">
						<xs:annotation>
							<xs:documentation>Association to fulfillment information relating to this order item. The fulfillment associations will evolve as the order progresses. Unreleased orders may have user/system selected or potential fulfillers. As the lifecycle of the order proceeds the associations will reflect the actual fulfillers and acts of fulfillment.</xs:documentation>
						</xs:annotation>
					</xs:element>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="CDSRationale" type="CDSRationale"/>
	<xs:complexType name="CDSRationale">
		<xs:annotation>
			<xs:documentation>Rational for the order is based on clinical decision support reasoning.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="CommonRationale">
				<xs:sequence>
					<xs:element name="BacktraceInformation" type="xs:string" minOccurs="1" maxOccurs="1"/>
					<xs:element name="Reasoning" type="Text" minOccurs="1" maxOccurs="1"/>
					<xs:element name="ReasoningEntity " type="Reference" minOccurs="1" maxOccurs="1"/>
					<xs:element name="ReasoningEntityType" type="ReasoningEntityType" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="FulfillmentOutcome" type="FulfillmentOutcome"/>
	<xs:complexType name="FulfillmentOutcome">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="OrderSummaryList" type="OrderSummaryList"/>
	<xs:complexType name="OrderSummaryList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="PolicyRationale" type="PolicyRationale"/>
	<xs:complexType name="PolicyRationale">
		<xs:annotation>
			<xs:documentation>Rationale for the order based on Policy, for example a reflex order.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="CommonRationale">
				<xs:sequence>
					<xs:element name="Organization" type="OrganizationReference" minOccurs="1" maxOccurs="1"/>
					<xs:element name="Description" type="xs:string" minOccurs="1" maxOccurs="1"/>
					<xs:element name="Identifiers" type="Identifier" minOccurs="0" maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="Promise" type="Promise"/>
	<xs:complexType name="Promise">
		<xs:annotation>
			<xs:documentation>This class represents the intent of the fulfillment to perform certain action(s).  This may or may not be the same as the requested action(s).</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="FulfillmentIdentity" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a fulfillment status</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderIdentifier" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a request for performance of order(s)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderedItems" type="OrderItem" minOccurs="1" maxOccurs="unbounded"/>
			<xs:element name="PromiseIdentity" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a promise or intent to perform.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="PromisedItems" type="OrderItem" minOccurs="0" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>The codes for the tests which the lab has an intent to perform.  This may be different than the ordered test codes.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Requirements" type="xs:string" minOccurs="0" maxOccurs="1"/>
			<xs:element name="PromiseState" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Subject" type="SubjectReference" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Order" type="Order" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderedItems" type="OrderItem" minOccurs="1" maxOccurs="unbounded"/>
			<xs:element name="PromisedItems" type="OrderItem" minOccurs="0" maxOccurs="unbounded"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderReleaseResult" type="OrderReleaseResult"/>
	<xs:complexType name="OrderReleaseResult">
		<xs:annotation>
			<xs:documentation>The results of attempting to release an order for fulfillment.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Released" type="xs:boolean" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Order" type="Order" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Faults" type="OrderFaults" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderSummaryInfo" type="OrderSummaryInfo"/>
	<xs:complexType name="OrderSummaryInfo">
		<xs:annotation>
			<xs:documentation>Summary information about an order.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="OrderIdentifier" type="Identifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Subject" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderedBy" type="ClinicalPractitionerReference" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Status" type="Coding" minOccurs="1" maxOccurs="1"/>
			<xs:element name="OrderTime" type="DateTime" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ItemTypes" type="Coding" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="PromissedItem" type="PromissedItem"/>
	<xs:complexType name="PromissedItem">
		<xs:annotation>
			<xs:documentation>Association to fulfillment information relating to this order item. The fulfillment associations will evolve as the order progresses. Unreleased orders may have user/system selected or potential fulfillers. As the lifecycle of the order proceeds the associations will reflect the actual fulfillers and acts of fulfillment.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="OrderItem">
				<xs:sequence>
					<xs:element name="FulfillmentOutcomes" type="FulfillmentOutcome" minOccurs="0" maxOccurs="unbounded"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="CareplanGoalRationale" type="CareplanGoalRationale"/>
	<xs:complexType name="CareplanGoalRationale">
		<xs:annotation>
			<xs:documentation>Rationale for the order is based on a care plan goal.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="CommonRationale">
				<xs:sequence>
					<xs:element name="Careplan" type="Identifier" minOccurs="1" maxOccurs="1"/>
					<xs:element name="Goal" type="Identifier" minOccurs="1" maxOccurs="1"/>
					<xs:element name="Description" type="xs:string" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
</xs:schema>
