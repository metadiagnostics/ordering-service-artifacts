<?xml version="1.0" encoding="utf-8"?>
<xs:schema  targetNamespace="http://www.omg.org/specs/HCOS/orderservice/types.xsd"  xmlns="http://www.omg.org/specs/HCOS/orderservice/types.xsd" xmlns:xs="http://www.w3.org/2001/XMLSchema">
	<xs:element name="OperationResult" type="OperationResult"/>
	<xs:complexType name="OperationResult">
		<xs:annotation>
			<xs:documentation>General operation returned structure to indicate success or failure and provided detailed information in the event of an operations failure.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Sucesss" type="xs:boolean" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ReasonForFailure" type="Coding" minOccurs="1" maxOccurs="1"/>
			<xs:element name="FailureDetail" type="xs:string" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Result" type="Result"/>
	<xs:complexType name="Result">
		<xs:annotation>
			<xs:documentation>For diagnostics like Laboratory, this class represents the 'answer' to the quantative or qualitative request.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="FulfillmentIdentity" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a fulfillment status</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="OrderIdentity" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of a request for performance of order(s)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ResultDetail" type="ResultDetail" minOccurs="1" maxOccurs="unbounded">
				<xs:annotation>
					<xs:documentation>The measurements and codified documentation of the lab results.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ResultIdentity" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Unique identifier for this instantiation of the documentation of diagnostic lab results.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ResultDetail" type="ResultDetail"/>
	<xs:complexType name="ResultDetail">
		<xs:annotation>
			<xs:documentation>The result details, qualitative and qualitative measurements.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Type" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Measurements made and documented using codes and interpretion.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Measurements which are numeric/units based.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Content" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Requirement" type="Requirement"/>
	<xs:complexType name="Requirement">
		<xs:annotation>
			<xs:documentation>Constraints or preconditions to the acceptance or fulfillment of an order. Requirements can be about the collection of a specimen, counseling, patient presence or procedure / sequence.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Identifier" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Type" type="RequirementType" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Hard Prerequisite - Reject if not present on create.
						Additional Workflow (e.g Analyte collection)</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Content" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="SearchParameterList" type="SearchParameterList"/>
	<xs:complexType name="SearchParameterList">
		<xs:sequence/>
	</xs:complexType>
	<xs:simpleType name="ReasoningEntityType">
		<xs:annotation>
			<xs:documentation>Enumeration defining the types of reasoning entity types.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:enumeration value="Human"/>
			<xs:enumeration value="Automated System"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="hexBinary" type="hexBinary"/>
	<xs:complexType name="hexBinary">
		<xs:annotation>
			<xs:documentation>A Base64 encoding of the binary content consistent with RFC 4648 Base64 encoding.</xs:documentation>
		</xs:annotation>
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="ClinicalPractitionerReference" type="ClinicalPractitionerReference"/>
	<xs:complexType name="ClinicalPractitionerReference">
		<xs:complexContent>
			<xs:extension base="Reference">
				<xs:sequence/>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="DateTime" type="DateTime"/>
	<xs:complexType name="DateTime">
		<xs:annotation>
			<xs:documentation>Date and Time stamp (TODO - Format needs to be specified)</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="value" type="xs:string" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ContentSignifier" type="ContentSignifier"/>
	<xs:complexType name="ContentSignifier">
		<xs:annotation>
			<xs:documentation>Defines the nature of the content.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Format" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The Format identifies the physical representation form the content.  For example as a HL7 XML V3 encoding, an HL7 FHIR JSON representation etc.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Type" type="xs:string" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="CodingList" type="CodingList"/>
	<xs:complexType name="CodingList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="ItemizedRequirement" type="ItemizedRequirement"/>
	<xs:complexType name="ItemizedRequirement">
		<xs:annotation>
			<xs:documentation>Union like structure Containing or relating to a specific requirement and it’s status. Used in reporting requirements.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Association" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Status" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Requirement" type="Requirement" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Type" type="xs:string" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="VersionedOperationResult" type="VersionedOperationResult"/>
	<xs:complexType name="VersionedOperationResult">
		<xs:annotation>
			<xs:documentation>An OperationResult supplemented with a VersionedIdentifier.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="OperationResult">
				<xs:sequence>
					<xs:element name="VersionedId" type="VersionedIdentifier" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="List" type="List"/>
	<xs:complexType name="List">
		<xs:sequence>
			<xs:element name="Items" type="xs:string" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Period" type="Period"/>
	<xs:complexType name="Period">
		<xs:annotation>
			<xs:documentation>A period of time. The start and end time if provided are consider inclusive.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="start" type="xs:string" minOccurs="0" maxOccurs="1"/>
			<xs:element name="end" type="xs:string" minOccurs="0" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="SearchOperatorType">
		<xs:annotation>
			<xs:documentation>Enumeration of Search operators types.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:enumeration value="ExactMatch"/>
			<xs:enumeration value="GreaterThan"/>
			<xs:enumeration value="LessThan"/>
			<xs:enumeration value="StartsWith"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="VersionedIdentifier" type="VersionedIdentifier"/>
	<xs:complexType name="VersionedIdentifier">
		<xs:annotation>
			<xs:documentation>An identifier supplemented with a version.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="Identifier">
				<xs:sequence>
					<xs:element name="Version" type="xs:int" minOccurs="1" maxOccurs="1"/>
				</xs:sequence>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="FaultList" type="FaultList"/>
	<xs:complexType name="FaultList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="Coding" type="Coding"/>
	<xs:complexType name="Coding">
		<xs:sequence>
			<xs:element name="System" type="xs:string" minOccurs="0" maxOccurs="1"/>
			<xs:element name="Version" type="xs:string" minOccurs="0" maxOccurs="1"/>
			<xs:element name="Code" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Display" type="xs:int" minOccurs="0" maxOccurs="1"/>
			<xs:element name="UserSelected" type="xs:boolean" minOccurs="0" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="ProposalList" type="ProposalList"/>
	<xs:complexType name="ProposalList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="SemanticVersion" type="SemanticVersion"/>
	<xs:complexType name="SemanticVersion">
		<xs:annotation>
			<xs:documentation>A semantic version identifier.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Major" type="xs:int" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Minor" type="xs:int" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Increment" type="xs:int" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Text" type="Text"/>
	<xs:complexType name="Text">
		<xs:annotation>
			<xs:documentation>Text represents textual information that is often in the form of documentation.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Signifier as to the form and nature of the encoded text </xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="Context" type="hexBinary" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The textual context in the form identifier in the ContentSignifier and placed in hexBinary form.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="RequirementAssociationType">
		<xs:annotation>
			<xs:documentation>Enumeration defining the types of requirement associations.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:enumeration value="Association"/>
			<xs:enumeration value="Requirement"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="CatalogEntrySummaryList" type="CatalogEntrySummaryList"/>
	<xs:complexType name="CatalogEntrySummaryList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="SearchParameter" type="SearchParameter"/>
	<xs:complexType name="SearchParameter">
		<xs:annotation>
			<xs:documentation>A parameter is search for. Include the value to check based in the enclosed operator.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Name" type="CodeableConcept" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Value" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Operator" type="SearchOperatorType" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrganizationReference" type="OrganizationReference"/>
	<xs:complexType name="OrganizationReference">
		<xs:annotation>
			<xs:documentation>A reference to an organization.</xs:documentation>
		</xs:annotation>
		<xs:complexContent>
			<xs:extension base="Reference">
				<xs:sequence/>
			</xs:extension>
		</xs:complexContent>
	</xs:complexType>
	<xs:element name="Provenance" type="Provenance"/>
	<xs:complexType name="Provenance">
		<xs:annotation>
			<xs:documentation>Represents the concept of provenance. This is a an envelope for a provenance representation.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Type" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="ContentSignifier" type="ContentSignifier" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Context" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:simpleType name="RequirementType">
		<xs:annotation>
			<xs:documentation>Enumeration defining the types of requirement type.</xs:documentation>
		</xs:annotation>
		<xs:restriction base="xs:string">
			<xs:enumeration value="Prerequisite"/>
			<xs:enumeration value="Fullfilment"/>
		</xs:restriction>
	</xs:simpleType>
	<xs:element name="ResultAugmentationList" type="ResultAugmentationList"/>
	<xs:complexType name="ResultAugmentationList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="NotificationLinkage" type="NotificationLinkage"/>
	<xs:complexType name="NotificationLinkage">
		<xs:annotation>
			<xs:documentation>Structure used to report on a potential linkage request between the local order service and a remote system.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ProvideUpdates" type="xs:boolean" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>If true the remote system should provide updates using the included Local identifier</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="LocalIdentifier" type="Identifier" minOccurs="0" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>Identifier provided by the Order system to identify this Order for the purposes of updates. Must be provided is ProvideUpdates is True.</xs:documentation>
				</xs:annotation>
			</xs:element>
			<xs:element name="RemoteIdentifier" type="Identifier" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>The Identifier provided by the remote system for this order.</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="URI" type="URI"/>
	<xs:complexType name="URI">
		<xs:annotation>
			<xs:documentation> A Uniform Resource Identifier Reference - See RFC 3986</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Value" type="xs:string" minOccurs="1" maxOccurs="1">
				<xs:annotation>
					<xs:documentation>xs:anyURI</xs:documentation>
				</xs:annotation>
			</xs:element>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="OrderableItemList" type="OrderableItemList"/>
	<xs:complexType name="OrderableItemList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="OrderableTemplateList" type="OrderableTemplateList"/>
	<xs:complexType name="OrderableTemplateList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="CatalogEntryList" type="CatalogEntryList"/>
	<xs:complexType name="CatalogEntryList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="ResultList" type="ResultList"/>
	<xs:complexType name="ResultList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="CodeableConcept" type="CodeableConcept"/>
	<xs:complexType name="CodeableConcept">
		<xs:sequence>
			<xs:element name="Coding" type="Coding" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="Text" type="xs:string" minOccurs="0" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="Identifier" type="Identifier"/>
	<xs:complexType name="Identifier">
		<xs:annotation>
			<xs:documentation>A numeric or alphanumeric string that is associated with a single object or entity within a given system.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="Use" type="xs:string" minOccurs="0" maxOccurs="1"/>
			<xs:element name="Type" type="CodeableConcept" minOccurs="1" maxOccurs="1"/>
			<xs:element name="System" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Value" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Period" type="Period" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Assigner" type="Reference" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
	<xs:element name="RequirementsList" type="RequirementsList"/>
	<xs:complexType name="RequirementsList">
		<xs:sequence/>
	</xs:complexType>
	<xs:element name="Reference" type="Reference"/>
	<xs:complexType name="Reference">
		<xs:annotation>
			<xs:documentation>A reference to an entity. Includes by identifier information as well as content based identification for error checking.</xs:documentation>
		</xs:annotation>
		<xs:sequence>
			<xs:element name="ReferenceType" type="Coding" minOccurs="1" maxOccurs="1"/>
			<xs:element name="DetailType" type="xs:string" minOccurs="1" maxOccurs="1"/>
			<xs:element name="Identities" type="xs:string" minOccurs="0" maxOccurs="unbounded"/>
			<xs:element name="Detail" type="xs:hexBinary" minOccurs="1" maxOccurs="1"/>
		</xs:sequence>
	</xs:complexType>
</xs:schema>
